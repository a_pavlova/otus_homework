﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_Console.Managers.Settings
{
    /// <summary>
    /// Класс для работы с настройками
    /// </summary>
    internal class SettingsManager : ISettingsManager
    {
        /// <summary>
        /// Настройки
        /// </summary>
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Имя секции
        /// </summary>
        private const string _sectionName = "Main";

        /// <summary>
        /// Имя параметра максимального числа
        /// </summary>
        private const string _maxNumberName = "MaxNumber";

        /// <summary>
        /// Имя параметра числа попыток
        /// </summary>
        private const string _triesCountName = "TriesCount";

        public SettingsManager(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Получить ограничение диапазона числа
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public int GetMaxNumber()
        {
            var value = _configuration.GetSection(_sectionName)?.GetSection(_maxNumberName)?.Value;

            if (int.TryParse(value, out int result))
            {
                return result;
            }
            else
            {
                throw new InvalidOperationException($"Параметр {_sectionName}.{_maxNumberName} введен некорректно");
            }
        }

        /// <summary>
        /// Получить ограничение диапазона числа
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public int GetTriesCount()
        {
            var value = _configuration.GetSection(_sectionName)?.GetSection(_triesCountName)?.Value;

            if (int.TryParse(value, out int result))
            {
                return result;
            }
            else
            {
                throw new InvalidOperationException($"Параметр {_sectionName}.{_triesCountName} введен некорректно");
            }
        }

    }
}
