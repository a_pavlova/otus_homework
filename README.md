# OTUS_Homework



## Getting started

SOLID

S - реализация разных классов для основной логики, для работы с настройками и для ввода числа

O - интерфейсы IMainLogic, ISettingsManager, IUserNumberManager. Реализацию интерфейса можно заменить, основной код будет работать так же.

L - LogicBase и все его наслденики обязаны реализовать метод Run()

I - интерфейс INumberManager имеет метод GetNumber. От него наследуется IUserNumberManager и добавлет еще один метод ValidateNumber. Получется, что класс UserNumberManager наследуется от IUserNumberManager и реалзует оба метода. А классу RandomMenedger нужен только метод GetNumber. Поэтому  он наследуется от INumberManager.

D - сделано. Все зависимости приходят в конструктор. И использую DI контейнер