﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_Console.Managers.Settings
{
    /// <summary>
    /// Интерфейс для работы с настройками
    /// </summary>
    internal interface ISettingsManager
    {
        /// <summary>
        /// Получить ограничение диапазона числа
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        int GetMaxNumber();

        /// <summary>
        /// Получить ограничение диапазона числа
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        int GetTriesCount();
    }
}
