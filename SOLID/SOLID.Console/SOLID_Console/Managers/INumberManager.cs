﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_Console.Managers
{
    /// <summary>
    /// Менеджер числа
    /// </summary>
    internal interface INumberManager
    {
        /// <summary>
        /// Получить число
        /// </summary>
        /// <returns>число</returns>
        int GetNumber(int maxValue);
    }
}
