﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_Console.Logics
{
    /// <summary>
    /// Интерфейс основной логики
    /// </summary>
    internal interface IMainLogic
    {
        /// <summary>
        /// Основной метод логики
        /// </summary>
        void Run();
    }
}
