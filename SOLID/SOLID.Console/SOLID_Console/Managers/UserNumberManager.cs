﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace SOLID_Console.Managers
{
    /// <summary>
    /// Менеджер ввода числа
    /// </summary>
    internal class UserNumberManager : IUserNumberManager
    {
        /// <summary>
        /// Получить число
        /// </summary>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        public int GetNumber(int maxValue)
        {
            var number = Console.ReadLine();

            if (Int32.TryParse(number, out int result))
            {
                if (result > 0 && result <= maxValue)
                {
                    return result;
                }
                else
                {
                    Console.WriteLine($"Число выходит за пределы диапазона от 1 до {maxValue}. Введите еще раз");
                }
            }
            else
            {
                Console.WriteLine($"Вы ввели некорректное число. Введите еще раз");
            }

            return GetNumber(maxValue);
        }

        /// <summary>
        /// Ввести число
        /// </summary>
        /// <param name="number">входящее число</param>
        /// <param name="maxNumber">Максимальное значение</param>
        /// <returns>Число</returns>
        public bool ValidateNumber(int number, int randomNumber)
        {
            if (number < randomNumber)
            {
                Console.WriteLine($"Загаданное число больше {number}. Введите число еще раз");
            }
            if (number > randomNumber)
            {
                Console.WriteLine($"Загаданное число меньше {number}. Введите число еще раз");
            }

            if (number == randomNumber)
            {
                Console.WriteLine($"Поздравляю! Вы угадали. Это число {randomNumber}");
                return true;
            }
            return false;
        }
    }
}
