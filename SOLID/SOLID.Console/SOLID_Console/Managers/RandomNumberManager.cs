﻿namespace SOLID_Console.Managers
{
    /// <summary>
    /// Менеджер рандомного числа
    /// </summary>
    internal class RandomNumberManager : INumberManager
    {
        /// <summary>
        /// Получить рандомное число
        /// </summary>
        /// <param name="maxValue">максимальное значение</param>
        /// <returns>рандомное число</returns>
        public int GetNumber(int maxValue)
        {
            Random random = new Random();
            return random.Next(maxValue);
        }
    }
}
