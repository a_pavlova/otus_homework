﻿using SOLID_Console.Managers;
using SOLID_Console.Managers.Settings;

namespace SOLID_Console.Logics
{
    /// <summary>
    /// Класс основной логики
    /// </summary>
    internal class MainLogic : LogicBase, IMainLogic
    {
        /// <summary>
        /// Настройки
        /// </summary>
        public readonly ISettingsManager _settings;

        /// <summary>
        /// Менеджер пользовательского числа
        /// </summary>
        public readonly IUserNumberManager _enterNumberManager;

        /// <summary>
        /// Менеджер рандомного числа
        /// </summary>
        public readonly INumberManager _numberManager;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="settings">Настройки</param>
        /// <param name="numberManager">Менеджер пользовательского числа</param>
        /// <param name="enterNumberManager">Менеджер рандомного числа</param>
        public MainLogic(ISettingsManager settings, IUserNumberManager enterNumberManager, INumberManager numberManager)
        {
            _settings = settings;
            _enterNumberManager = enterNumberManager;
            _numberManager = numberManager;
        }

        /// <summary>
        /// Основной метод логики
        /// </summary>
        public override void Run()
        {
            var maxNumber = _settings.GetMaxNumber();
            var triesCount = _settings.GetTriesCount();

            //Получить рандомное число
            var num = _numberManager.GetNumber(maxNumber);

            Console.WriteLine($"Попробуйте угадать число от 1 до {maxNumber}");

            for (int i = 0; i < triesCount; i++)
            {
                var number = _enterNumberManager.GetNumber(maxNumber);

                if (_enterNumberManager.ValidateNumber(number, num))
                {
                    return;
                }
            }

            Console.WriteLine($"К сожалению, вы не угадали число {num}");
        }
    }
}
