﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SOLID_Console.Logics;
using SOLID_Console.Managers;
using SOLID_Console.Managers.Settings;

namespace SOLID_Console
{
    /// <summary>
    /// Расширение для сервиса
    /// </summary>
    internal static class ServiceExtension
    {
        /// <summary>
        /// Добавить конфигурацию
        /// </summary>
        /// <param name="services"></param>
        public static void AddConfiguration(this IServiceCollection services)
        {
            var configBuilder = new ConfigurationBuilder();

            configBuilder.SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            IConfiguration config = configBuilder.Build();

            services.AddSingleton(config);
        }

        /// <summary>
        /// Добавить зависимости
        /// </summary>
        /// <param name="services"></param>
        public static void AddDependencies(this IServiceCollection services)
        {           
            services.AddSingleton<ISettingsManager, SettingsManager>();
            services.AddSingleton<IUserNumberManager, UserNumberManager>();
            services.AddSingleton<INumberManager, RandomNumberManager>();
            services.AddSingleton<IMainLogic, MainLogic>();
        }
    }
}
