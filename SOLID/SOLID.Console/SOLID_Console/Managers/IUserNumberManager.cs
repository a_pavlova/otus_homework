﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_Console.Managers
{
    /// <summary>
    /// Интерфейс для ввода числа
    /// </summary>
    internal interface IUserNumberManager: INumberManager
    {
        /// <summary>
        /// Ввести число
        /// </summary>
        /// <param name="maxNumber">Входящее число</param>
        /// <param name="maxNumber">Максимальное значение</param>
        /// <returns>Число</returns>
        bool ValidateNumber(int number, int maxNumber);
    }
}
