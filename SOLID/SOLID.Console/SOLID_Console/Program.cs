﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SOLID_Console.Logics;

namespace SOLID_Console
{
    internal class Program
    {
        static void Main(string[] args)
        {
            HostApplicationBuilder builder = Host.CreateApplicationBuilder(args);

            // Добавление конфигурации
            builder.Services.AddConfiguration();
            // Добавление зависимостей
            builder.Services.AddDependencies();

            var host = builder.Build();

            // Получить зарегистрированный класс основной логики
            IServiceScope serviceScope = host.Services.CreateScope();
            IServiceProvider provider = serviceScope.ServiceProvider;
            IMainLogic mainLogic = provider.GetRequiredService<IMainLogic>();

            // Запуск основной логики
            mainLogic.Run();
        }
    }
}
