﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_Console.Logics
{
    /// <summary>
    /// Базовый класс для логики
    /// </summary>
    internal abstract class LogicBase
    {
        /// <summary>
        /// Общий метод для всех наследников
        /// </summary>
        public abstract void Run();
    }
}
